package com.example.nyc_school_acc.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NycApp : Application()