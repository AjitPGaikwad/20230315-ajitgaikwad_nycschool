package com.example.nyc_school_acc.common

sealed class StateAction {
    class SUCCESS<T>(val response: T) : StateAction()
    class ERROR(val error: Exception) : StateAction()
}
