package com.example.nyc_school_acc.common

import com.example.nyc_school_acc.domain.SchoolDomain

interface OnSchoolClicked {
    fun schoolClicked(school: SchoolDomain)
}