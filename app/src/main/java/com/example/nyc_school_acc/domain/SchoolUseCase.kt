package com.example.nyc_school_acc.domain

import javax.inject.Inject

class SchoolUseCase @Inject constructor(
    private val repository: Repository
) {
    operator fun invoke() = repository.NYCSchoolCatched()
}