package com.example.nyc_school_acc.domain

import com.example.nyc_school_acc.common.InternetCheck
import com.example.nyc_school_acc.common.StateAction
import com.example.nyc_school_acc.model.local.LocalDataSource
import com.example.nyc_school_acc.model.remote.RemoteDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

interface Repository {
    fun NYCSchoolCatched(): Flow<StateAction>
    fun NYCSatCatched(schoolDbn: String): Flow<StateAction>
}

class RepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : Repository {

    override fun NYCSchoolCatched(): Flow<StateAction> = flow {
        val connected = InternetCheck()
        val remoteService = remoteDataSource.NYCSchoolCatched()
        if (connected.isConnected()) {
            remoteService.collect() { stateAction ->
                when (stateAction) {
                    is StateAction.SUCCESS<*> -> {
                        val retrievedSchools = stateAction.response as List<SchoolDomain>
                        emit(StateAction.SUCCESS(retrievedSchools))
                        localDataSource.insertLocalSChool(retrievedSchools).collect()

                    }
                    is StateAction.ERROR -> TODO()
                }
            }
        } else {
            val cache = localDataSource.getAllCachedSchools()
            cache.collect(){ stateAction ->
                when (stateAction) {
                    is StateAction.SUCCESS<*> -> {
                        val retrievedSchools = stateAction.response as List<SchoolDomain>
                        emit(StateAction.SUCCESS(retrievedSchools))
                    }
                    is StateAction.ERROR -> TODO()
                }
            }
        }
    }

    override fun NYCSatCatched(schoolDbn: String): Flow<StateAction> = flow {
        val connected = InternetCheck()
        val remoteService = remoteDataSource.NYCSatToRoom()
        if (connected.isConnected()) {
            remoteService.collect() { stateAction ->
                when (stateAction) {
                    is StateAction.SUCCESS<*> -> {
                        val retrievedSat = stateAction.response as List<SatDomain>
                        localDataSource.insertLocalSat(retrievedSat).collect()
                        emit(StateAction.SUCCESS(retrievedSat.filter { x ->
                            x.dbn == schoolDbn
                        }))
                    }
                    is StateAction.ERROR -> TODO()
                }
            }
        } else {
            val cache = localDataSource.getAllCachedSat()
            cache.collect(){ stateAction ->
                when (stateAction){
                    is StateAction.SUCCESS<*> -> {
                        val retrievedSat = stateAction.response as List<SatDomain>
                        emit(StateAction.SUCCESS(retrievedSat.filter { x ->
                            x.dbn == schoolDbn
                        }))
                    }
                    is StateAction.ERROR -> TODO()
                }

            }
        }
    }
}