package com.example.nyc_school_acc.domain

import javax.inject.Inject

class SatUseCase @Inject constructor(
    private val repository: Repository
) {
    operator fun invoke(schoolDbn: String) = repository.NYCSatCatched(schoolDbn)
}